import { Component, OnInit, ElementRef,AfterViewChecked, ViewChild } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SpeechToTextService} from '../speech-to-text.service';
import {ContentType} from '@angular/http/src/enums';

@Component({
  selector: 'app-front-page',
  templateUrl: './front-page.component.html',
  styleUrls: ['./front-page.component.css']
})
// declare var particlesJS: any;

export class FrontPageComponent implements OnInit, AfterViewChecked {
  @ViewChild('scroller') private feedContainer:ElementRef;
  time = 0;
  messages = [];
  _url = '';
  speechData: string = '';
  text: any;
  text2: any;
  split: any;
  showWindow = false;
  name = '';
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  
  // particlesJS = any;
  headers: HttpHeaders = new HttpHeaders().set('ContentType', 'application/json');
  constructor(private httpClient: HttpClient, private speechRecognition: SpeechToTextService) {

  }

  ngOnInit() {
    this.myStyle = {
      'position': 'fixed',
      'width': '100%',
      'height': '100%',
      'z-index': 0,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
  };

  this.myParams = {
          particles: {
              number: {
                  value: 60,
              },
              color: {
                  value: '#ffffff'
              },
              shape: {
                  type: 'circle',
              },
              stroke: {
                width: 3,
                color: ' #ffffff'
              },
              line_linked:{
                enabled:true,
                distance:110,
                color:'#ffffff'
            },
      }
  };

        this.scrollToBottom();
        

    }

   ngAfterViewChecked() {
        this.scrollToBottom();
    }

   scrollToBottom(): void {
        try {
            this.feedContainer.nativeElement.scrollTop = this.feedContainer.nativeElement.scrollHeight;
        } catch(err) { }
    }



  sendMessage(msg)
  {
    this.speechData = '';
    if (msg !== '') {
      // const translate = require('google-translate-api');
      // translate(msg, {from: 'en', to: 'en'}).then(res => {
      //   const crctmsg = res.from.text.value;
      // });
      this.messages.push({id: 'usr', text: msg });
      this._url = 'http://139.59.59.109:5001/conversation/' + msg;

      this.httpClient.get(this._url, {headers: this.headers.append('Access-Control-Allow-Origin', '*')
      })
        .subscribe(
          (data: any) => {
            /*var words = msg.splits(' ');*/

              console.log(this._url);
              console.log('data hai ' + data);
              console.log('msg hai ' + msg);
              console.log(data);
              
              // this.messages.push({id: 'bot', text:data.split("::")[0] , text2: data.split("::")[1]});
              // this.TextToSpeechMethod(data.split("::")[0]);
              this.messages.push({id: 'bot', text:data.split("::")[0] , text2: data.split("::")[1]});
              this.TextToSpeechMethod(data.split("::")[0]);
              const ul = document.getElementById('ChatList');
              ul.scrollTop = ul.scrollHeight;
              
          /*  if(data.length) {
              console.log(this._url);
              console.log('data hai ' + data);
              console.log('msg hai ' + msg);
              this.messages.push({id: 'bot', text: data});
              this.TextToSpeechMethod(data);
              const ul = document.getElementById('ChatList');
              ul.scrollTop = ul.scrollHeight;
            } else{
              console.log('this is console');
              this.messages.push({id: 'bot', text: map.get(msg)});
            }*/
          }
        );
      const ul = document.getElementById('ChatList');
      ul.scrollTop = ul.scrollHeight;
    }
  }


  SpeechToText()
  {
    this.speechRecognition.record()
      .subscribe(
        (value) =>{
          this.speechData = value;
          if(this.speechData !== '')
          {
            this.sendMessage(this.speechData);
          }

        }
      );
  }

  TextToSpeechMethod(reply)
  {
      const speech = new SpeechSynthesisUtterance(reply);
      window.speechSynthesis.speak(speech);

  }


  ToggleWindow(){
   /*window.open('chatbox', '_blank', 'directories=no,titlebar=no,toolbar=no,status=no,menubar=no,location=no,height=450,width=400,scrollbars=yes,status=no, fullscreen = yes');*/
   this.showWindow = !this.showWindow;
 }
 // scrollToBottom(this)

}
