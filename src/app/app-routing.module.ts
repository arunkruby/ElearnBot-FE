import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FrontPageComponent} from './front-page/front-page.component';
// import {ChatWindowComponent} from './chat-window/chat-window.component';
// import {ChatBoxComponent} from './chat-box/chat-box.component';

const routes: Routes = [
    { path : '', redirectTo : '/frontpage', pathMatch : 'full' },
  { path : 'frontpage', component: FrontPageComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
