import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { FrontPageComponent } from './front-page/front-page.component';
// import { ChatWindowComponent } from './chat-window/chat-window.component';

import {SpeechToTextService} from './speech-to-text.service';
// import { ChatBoxComponent } from './chat-box/chat-box.component';
import { ParticlesModule } from 'angular-particle';

@NgModule({
  declarations: [
    AppComponent,
    FrontPageComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ParticlesModule,
  ],
  providers: [SpeechToTextService],
  bootstrap: [AppComponent]
})
export class AppModule { }
