import { Injectable, NgZone } from '@angular/core';

interface IWindow extends Window {
  webkitSpeechRecognition: any;
  speechSynthesis: any;
}

@Injectable()
export class TextToSpeechService {

  constructor() { }

}
